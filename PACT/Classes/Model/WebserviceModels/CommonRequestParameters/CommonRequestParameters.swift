//
//  CommonRequestParameters.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation
import UIKit

class CommonRequestParameters{
    
//    var deviceToken = Globals.sharedInstance.deviceToken
    var deviceType = "iPhone"
//    static let deviceId = Utilities.sharedInstance.getDeviceID()
    var deviceResolution = UIScreen.main.scale>2.0 ? "3x" :"2x"
    var appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    var deviceKey = ""
}
