//
//  User.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var userID:Int? = 0
    var userName:String? = ""
    var email:String? = ""
    var password:String? = ""
    var confirmPassword:String? = ""
    var firstName:String? = ""
    var lastName:String? = ""
    var contactNumber:String? = ""
    var address1:String? = ""
    var address2:String? = ""
    var zipCode:String? = ""
    var city:String? = ""
    var state:String? = ""
    var profilePicture = ""
    
    override init () {
        // uncomment this line if your class has been inherited from any other class
        super.init()
    }
    
    
    required init(coder aDecoder: NSCoder) {
//        if let userName = aDecoder.decodeObject(forKey: "name") as? String {
//            self.name = userName
//        }
//        if let userEmail = aDecoder.decodeObject(forKey: "emailId") as? String{
//            self.emailId = userEmail
//        }
//        if let phoneNumber = aDecoder.decodeObject(forKey: "contactNumber") as? String{
//            self.contactNumber = phoneNumber
//        }
//        if let currentUserId = aDecoder.decodeObject(forKey: "userId") as? String{
//            self.userId = currentUserId
//        }
//        if let profilePicture = aDecoder.decodeObject(forKey: "profilePicture") as? String
//        {
//            self.profilePicture = profilePicture
//        }
//        self.isVerified =  aDecoder.decodeBool(forKey: "isVerified")
//        self.isRegistered =  aDecoder.decodeBool(forKey: "isRegistered")
//        
    }
//
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let userName = self.userName {
            aCoder.encode(userName, forKey: "userName")
        }
        if let email =  self.email{
            aCoder.encode(email, forKey: "emailId")
        }

//        aCoder.encode(self.profilePicture, forKey: "profilePicture")
//        
//        aCoder.encode(self.userId, forKey: "userId")
//        aCoder.encode(self.isRegistered, forKey: "isRegistered")
//        aCoder.encode(self.isVerified, forKey: "isVerified")
        
    }
    
    convenience init(dictionary: Dictionary<String, AnyObject>) {
        self.init()
      
        self.userName = dictionary[WebServicesKeys.USERNAME] != nil && dictionary[WebServicesKeys.USERNAME] is String ? dictionary[WebServicesKeys.USERNAME] as! String : ""

        self.email = dictionary[WebServicesKeys.EMAIL_ID] != nil && dictionary[WebServicesKeys.EMAIL_ID] is String ? dictionary[WebServicesKeys.EMAIL_ID] as! String : ""
        
        self.profilePicture = dictionary[WebServicesKeys.PROFILE_PICTURE] != nil && dictionary[WebServicesKeys.PROFILE_PICTURE] is String ? dictionary[WebServicesKeys.PROFILE_PICTURE] as! String: ""
       
//        self.userID = dictionary[WebServicesKeys.USER_ID] != nil && dictionary[WebServicesKeys.USER_ID] is String ? dictionary[WebServicesKeys.USER_ID] as! String : ""

        if(dictionary["userDetails"] != nil){
        }
        
    }
    
    func parseUserDetails(_ dictionary:Dictionary<String,AnyObject>){
        self.email = dictionary[WebServicesKeys.EMAIL_ID] != nil && dictionary[WebServicesKeys.EMAIL_ID] is String ? dictionary[WebServicesKeys.EMAIL_ID] as! String: ""
        self.userName = dictionary[WebServicesKeys.USERNAME] != nil && dictionary[WebServicesKeys.USERNAME] is String ? dictionary[WebServicesKeys.USERNAME] as! String: ""
        self.profilePicture = dictionary[WebServicesKeys.PROFILE_PICTURE] != nil && dictionary[WebServicesKeys.PROFILE_PICTURE] is String ? dictionary[WebServicesKeys.PROFILE_PICTURE] as! String: ""
    }

}
