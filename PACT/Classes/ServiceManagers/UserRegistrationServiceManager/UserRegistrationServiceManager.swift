//
//  UserRegistrationServiceManager.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation

class UserRegistrationServiceManager{
    fileprivate init(){}
    
    static let sharedInstance = UserRegistrationServiceManager()
    
    //Send Request to webservice for user registration
    func sendRequestForUserLogin(_ paramsDictionary:[String:AnyObject], successCallback: @escaping (AnyObject) -> Void, failureCallback:@escaping (NSError)->Void) -> Void {
        let manager = BaseServiceManager.sharedInstance
        
        let requestObject = manager.getRequestObjectFor("api/accountapp/login", requestParametersDictionary: paramsDictionary as NSDictionary)
        manager.sendRequestToServerWith(requestObject, successCallback:{(response) in
            successCallback(response)
        } , failureCallback: {(error) in
            failureCallback(error)
        })
    }
    
    func sendRequestForRegistrationWith(_ paramsDictionary:[String:AnyObject], successCallback: @escaping (AnyObject) -> Void, failureCallback:@escaping (NSError)->Void) -> Void {
        let manager = BaseServiceManager.sharedInstance
        
        let requestObject = manager.getRequestObjectFor("api/accountapp/register", requestParametersDictionary: paramsDictionary as NSDictionary)
        manager.sendRequestToServerWith(requestObject, successCallback: { (response) in
            successCallback(response)
        }, failureCallback: {(error) in
            failureCallback(error)
        })
    }
}
