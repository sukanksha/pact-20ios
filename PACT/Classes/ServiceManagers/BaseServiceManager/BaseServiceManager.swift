//
//  BaseServiceManager.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation
import Alamofire

class BaseServiceManager {
    fileprivate init(){}
    
    static let sharedInstance = BaseServiceManager()
    
    func getRequestObjectFor(_ requestURL:String, requestParametersDictionary:NSDictionary) -> URLRequest{
        var urlString = "\(WebserviceConstants.baseURL)/\(requestURL)"
        urlString = urlString.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let  url = URL( string: urlString)!
        let request = NSMutableURLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod="POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: requestParametersDictionary, options: [])
        } catch let error as NSError {
            request.httpBody = nil
            print("\(error.description)")
        }
        return request as URLRequest
    }
    
    func sendRequestToServerWith(_ urlRequest:URLRequest, successCallback: @escaping (AnyObject) -> Void, failureCallback:@escaping (NSError)->Void) -> Void {
        
        Alamofire.request(urlRequest).responseJSON { response in
            //            print(response.request)  // original URL request
            //            print(response.response) // HTTP URL response
            //            print(response.data)     // server data
            print(response.result)   // result of response serialization
            if ((response.error) != nil) {
                print("Error: \(response.error)")
                failureCallback(response.error! as NSError)
            } else {
                print("Response: \(response)")
                successCallback(response.result.value as AnyObject)
            }
            if let JSON = response.result.value {
                let isSuccess = response.result.isSuccess
                print("JSON: \(JSON)")
            }
        }
    }
    //        manager.responseSerializer=AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments)
    //        let task = manager.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) in
    //            // this is where the completion handler code goes
    //            if ((error) != nil) {
    //                print("Error: \(error)")
    //                failureCallback(error! as NSError)
    //            } else {
    //                print("Response: \(response)")
    //                successCallback(response! as AnyObject)
    //            }
    //        })
    //        task.resume()
}
