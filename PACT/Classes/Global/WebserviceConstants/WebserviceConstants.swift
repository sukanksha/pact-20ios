//
//  WebserviceConstants.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation

struct WebserviceConstants {
    static let baseURL = "http://192.168.88.217:8085/"
    static let ACCESS_TOKEN_KEY = "accessToken"
    static let DEVICE_TOKEN_KEY = "deviceToken"
    static let DEVICE_TYPE_KEY = "deviceType"
    static let DEVICE_ID_KEY = "deviceId"
    static let STORYBOARD_NAME = "Main"
    static let CANCEL = "Cancel"
    static let OK = "OK"
}


struct ViewControllers {
    static let NOTIFICATION_VIEW_CONTROLLER = "NotificationViewController"
    static let USER_PROFILE_VIEW_CONTROLLER = "UserProfileViewController"
    static let TAB_VIEW_CONTROLLER = "TabViewController"
    static let PENDING_REQUEST_VC = "PendingRequestVC"
    static let ABOUT_VIEW_CONTROLLER = "AboutViewController"
    static let PRIVACY_TERMS_CONDITIONS_VC = "PrivacyTermsAndConditionsVC"
    static let FEEDBACK_VC = "FeedbackVC"
    static let ARCHIVED_GROUP_TVC = "ArchivedGroupTableViewController"
    static let CREATE_GROUP_PROFILE_VC = "CreateGroupProfileViewController"
    static let GROUP_DETAILS_VC = "GroupDetailsViewController"
    static let ADD_GROUP_MEMBERS_VC = "AddGroupMembersViewController"
    static let MAP_VC = "MapViewController"
    static let APPDIRECTORY_VC = "AppDirectoryViewController"
    static let VERIFY_OTP_VC = "VerifyOTPViewController"
    static let SHOW_SIGN_UP_VC = "showSignUpVC"
}

struct TableViewCell {
    static let PENDING_USER_CELL = "PendingUserCell"
    static let SETTINGS_TABLEVIEWCELL = "SettingsTableViewCell"
    static let NOTIFICATION_TABLEVIEWCELL = "NotificationTableViewCell"
    static let ARCHIVED_CELL = "ArchivedCell"
    static let CREATE_GROUP_CELL = "CreateGroupCell"
    static let GROUP_MEMBER_CELL = "GroupMemberCell"
    static let GROUP_MEMBER_COLLECTION_VC = "GroupMemberCollectionViewCell"
    static let DELETE_LEAVE_CELL = "deleteLeaveGroupCell"
    static let BLOCKED_USER_CELL = "BlockedUserCell"
    static let REQUESTED_USER_CELL = "RequestedUserCell"
    static let APP_USER_CELL = "AppUserCell"
    static let TRACKABLE_USER_CELL = "TrackableUserCell"
    static let APPDIRECTORY_CELL = "appDirectoryCell"
    static let SEARCH_LOCATION_DETAIL_VIEW = "SearchLocationDetailsView"
}
struct WebServicesKeys {
    static let USER_ID = "userId"
    static let USERNAME = "username"
    static let EMAIL_ID = "Email"
    static let PASSWORD = "password"
    static let confirmPassword = "ConfirmPassword"
    static let FIRST_NAME = "FirstName"
    static let LAST_NAME = "LastName"
    static let PHONE_NUMBER = "PhoneNumber"
    static let ADDRESS1 = "Address1"
    static let ADDRESS2 = "Address2"
    static let ZIP_CODE = "ZipCode"
    static let CITY = "City"
    static let STATE = "State"
    static let STATUS = "Status"
    static let ITEM = "Item"
    static let PROFILE_PICTURE = "ProfileImage"
    static let IS_SUCCESS = "IsSuccess"
}
struct ScreenNames {
    static let LOGOUT = "Logout"
    static let EDIT_PROFILE_SCREEN = "Edit Profile"
    static let PROFILE_SCREEN = "Profile"
    static let SEND_OTP_SCREEN = "Sign In"
    static let VERIFY_OTP_SCREEN = "OTP"
    static let HOME_SCREEN = "Home"
    static let APP_DIRECTORY_SCREEN = "Contacts"
    static let DIALER_SCREEN = "Dialer"
    static let SETTINGS_SCREEN = "Settings"
    static let GROUPS_SCREEN = "Groups"
    static let ARCHIVED = "Archived Groups"
    static let CREATE_GROUP_PROFILE = "Create Group"
    static let ADD_GROUP_MEMBERS = "Add Members"
    static let ARCHIVED_SCREEN = "Archived Groups" //SSL:added
    static let CLONE_SCREEN = "Clone Group" //SSL:added
    static let PENDING_REQUEST_SCREEN = "Pending Requests"
    static let MY_PROFILE = "My Profile"
    static let FEEBACK_SCREEN = "Feedback"
    static let PRIVACY_POLICY = "Privacy Policy"
    static let TERMS_AND_CONDITIONS = "Terms and Conditions"
    static let HELP_SCREEN = "Help"
    static let ABOUT = "About"
    static let SHARE = "Share"
    static let RATE = "Rate"
}

