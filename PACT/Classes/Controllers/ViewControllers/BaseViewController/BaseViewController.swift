//
//  BaseViewController.swift
//  PACT
//
//  Created by Sukanksha on 31/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController:UIViewController{
    
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setNavigationAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func addBarButtonItems(){
        
    }
    
}
