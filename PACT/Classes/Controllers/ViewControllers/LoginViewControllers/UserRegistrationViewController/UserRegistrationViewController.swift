//
//  UserRegistrationViewController.swift
//  PACT
//
//  Created by Sukanksha on 31/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation
import UIKit

class UserRegistrationViewController:BaseViewController,UITextFieldDelegate,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var firstNameTxtField: UITextField!
    
    @IBOutlet weak var lastNameTxtField: UITextField!
    
    @IBOutlet weak var passwordTxtField: UITextField!
    
    @IBOutlet weak var confirmPasswordTxtField: UITextField!
    
    @IBOutlet weak var emailTxtField: UITextField!
    
    @IBOutlet weak var contactNumberTxtField: UITextField!
    
    @IBOutlet weak var addressLine1TxtField: UITextField!
    
    @IBOutlet weak var addressLine2TxtField: UITextField!
    
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var zipCodeTxtField: UITextField!

    @IBOutlet weak var stateTxtField: UITextField!

    @IBOutlet weak var registrationScrollView: UIScrollView!
    
    @IBOutlet weak var registrationView: UIView!
    
    var keyboardFrame:CGRect!
    var activeTxtField:UITextField!
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Registration form"
        setUpView()
        
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    func setUpView(){
        
        //add tap gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)

        //set delegate
        firstNameTxtField.delegate = self
        lastNameTxtField.delegate = self
        
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
    
        confirmPasswordTxtField.delegate = self
        
        contactNumberTxtField.delegate = self
        addressLine1TxtField.delegate = self
        
        zipCodeTxtField.delegate = self
        stateTxtField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(UserRegistrationViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UserRegistrationViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: TextField Dalegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTxtField = textField
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag=textField.tag+1;
        // Try to find next responder
        let nextResponder=textField.superview?.viewWithTag(nextTag) as UIResponder!
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return false
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    //MARK: Login
    func registerUser(){
        let userObj = User()
        userObj.email = self.emailTxtField.text
        userObj.password = self.passwordTxtField.text
        userObj.confirmPassword = self.confirmPasswordTxtField.text
        userObj.firstName = self.firstNameTxtField.text
        userObj.lastName = self.lastNameTxtField.text
        userObj.address1 = self.addressLine1TxtField.text
        userObj.address2 = self.addressLine2TxtField.text
        userObj.zipCode = self.zipCodeTxtField.text
        userObj.state = self.stateTxtField.text
        userObj.city = self.cityTxtField.text
        
        if Utilities.isConnectedToInternet() {
            print("Yes! internet is available.")
            UserRegistrationDataManager.sharedInstance.registerUser(userObj, successCallback: {
                errorCode, isSuccess, message, isVerified,user in
                
                if isSuccess{
                    //parse the json
                }else{
                    //show the alert with an error
                }
                
            }, failureCallback: {
                error in
            })
        }
        
    }
    
    
    @IBAction func registrationDoneButtonTapped(_ sender: Any) {
        registerUser()
    }
    
    //MARK:- Keyboard Methods
    func keyboardWillShow(_ notification:Foundation.Notification){
        registrationScrollView.isScrollEnabled = true
        
        var userInfo = notification.userInfo!
        keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = self.registrationScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height+100
        
        print(keyboardFrame.size.height)
        self.registrationScrollView.contentInset = contentInset
    }
    func addDoneButtonOnKeyBoard() -> Void {
//        let keyboardDoneButtonView = UIToolbar.init()
//        keyboardDoneButtonView.sizeToFit()
//        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done,
//                                              target: self,
//                                              action: #selector(VerifyOTPViewController.doneButtonTapped))
//        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
//                                            target: nil, action: nil)
//        keyboardDoneButtonView.items = [flexBarButton,doneButton]
//        contactNumberTxtField.inputAccessoryView = keyboardDoneButtonView
    }
    
    func doneButtonTapped() -> Void {
        self.view.endEditing(true)
    }
    
    func keyboardWillHide(_ notification:Foundation.Notification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.registrationScrollView.contentInset = contentInset
        registrationScrollView.isScrollEnabled = false
        
    }

    
    
}
