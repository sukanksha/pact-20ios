//
//  LoginViewController.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var emailTxtField: UITextField!
    
    @IBOutlet weak var passwordTxtField: UITextField!
    
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var logoViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var logoImageTopConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        
        setUpView()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func setUpView()
    {
        emailView.layer.cornerRadius = 8.0
        passwordView.layer.cornerRadius = 8.0
        signInBtn.layer.cornerRadius = 14.0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    func loginUser(){
        
    }
    
    //MARK :-  Button Actions
   
    @IBAction func registerButtonClicked(_ sender: Any) {
    }
    
    @IBAction func signInButtonClicked(_ sender: Any) {
        if !((emailTxtField.text?.isEmpty)!) && !((passwordTxtField.text?.isEmpty)!){
            let userObj = User()
            userObj.email = self.emailTxtField.text
            userObj.password = self.passwordTxtField.text
            
            if Utilities.isConnectedToInternet() {
                print("Yes! internet is available.")
                UserRegistrationDataManager.sharedInstance.loginUser(userObj, successCallback: {
                    isSuccess, message,user in
                    if(isSuccess){
                        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.navigationController?.pushViewController(homeViewController, animated: true)
                    }
                }, failureCallback: {
                    error in
                })
            }
        }
    }
  


    //MARK :- UITextFieldDelegate Methods
    func textFieldDidEndEditing(_ textField:UITextField){
        
        if(!(emailTxtField.text?.isEmpty)! && !(passwordTxtField.text?.isEmpty)!){
            signInBtn.isEnabled = true
            signInBtn.alpha = 1.0
        }else{
            signInBtn.isEnabled = false
            signInBtn.alpha = 0.3
//            //This is the code for view up
//            UIView.animate(withDuration: 0.45, animations: {
//                self.logoImageTopConstraint.constant = 0
//            })

        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == emailTxtField){
            passwordTxtField.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case emailTxtField:
            //This is the code for view up
            UIView.animate(withDuration: 0.45, animations: {
                self.logoImageTopConstraint.constant = -200
            })
        default:
            //This the code for view down
            UIView.animate(withDuration: 0.45, animations: {
                self.logoImageTopConstraint.constant = 0
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var valid :Bool = true
//        if textField == passwordTxtField{
//            let newlenth = textField.text!.utf16.count + string.utf16.count - range.length
//            valid = newlenth <= 8
//        }
        return valid
        
    }

    //MARK:- Keyboard Methods
    func keyboardWillShow(_ notification:Foundation.Notification){
        
    }
    
    func keyboardWillHide(_ notification:Foundation.Notification){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
