//
//  HomeViewController.swift
//  PACT
//
//  Created by Sukanksha on 02/06/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController:BaseViewController{
    
    @IBOutlet weak var vehicleInformationView: UIView!
    
    @IBOutlet weak var reviewCitationView: UIView!
  
    @IBOutlet weak var searchPermitView: UIView!
    
    @IBOutlet weak var logoutView: UIView!

    @IBOutlet weak var beginCitationView: UIView!
    
    
    @IBOutlet weak var profileImageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = ScreenNames.HOME_SCREEN
         setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView(){
        self.navigationController?.navigationBar.tintColor = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1.0)
    
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor(red: 172.0/255, green: 203.0/255, blue: 18.0/255, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica", size: 17.0)!]
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        self.vehicleInformationView.layer.cornerRadius = self.vehicleInformationView.frame.size.width/2
        self.reviewCitationView.layer.cornerRadius = self.reviewCitationView.frame.size.width/2
        self.searchPermitView.layer.cornerRadius = self.searchPermitView.frame.size.width/2
        self.logoutView.layer.cornerRadius = self.logoutView.frame.size.width/2
        self.beginCitationView.layer.cornerRadius = self.beginCitationView.frame.size.width/2
    }
}
