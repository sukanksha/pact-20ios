//
//  UserRegistrationDataManager.swift
//  PACT
//
//  Created by Sukanksha on 24/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation

class UserRegistrationDataManager{
    static let sharedInstance = UserRegistrationDataManager()
    
    func loginUser(_ user:User, successCallback: @escaping (Bool, String,User) -> Void, failureCallback:@escaping (NSError)->Void) -> Void {
        _ = CommonRequestParameters()
        let paramsDictionary:[String:String] = [WebServicesKeys.USERNAME:user.email!,
                                                WebServicesKeys.PASSWORD:user.password!]
        UserRegistrationServiceManager.sharedInstance.sendRequestForUserLogin(paramsDictionary as [String : AnyObject], successCallback: {
            response in

            let errorCode = response.value(forKey: WebServicesKeys.STATUS)
            let (isSuccess,userdetails) = self.parseUser(response)
            successCallback(isSuccess,String(describing: errorCode),userdetails)
            
        },failureCallback: {
            error in
            failureCallback(error)
        })
    }
    
    func parseUser(_ response: AnyObject) ->(Bool,User)
    {
        print(response)
        let isSuccess = response.value(forKey:WebServicesKeys.IS_SUCCESS) as! Bool
//        let message = response.object(forKey: WebServicesKeys.MESSAGE) as! String
        
        var userDetail  = User()
        if isSuccess
        {
            let userdata = response.object(forKey: WebServicesKeys.ITEM) as! Dictionary<String,AnyObject>
            let user = User(dictionary: userdata)
            userDetail = user
//            userDetail.userID = response.object(forKey: WebServicesKeys.USER_ID) as! String
            userDetail.email = userdata[WebServicesKeys.EMAIL_ID] as? String
            userDetail.profilePicture = userdata[WebServicesKeys.PROFILE_PICTURE] != nil && userdata[WebServicesKeys.PROFILE_PICTURE] is String ? userdata[WebServicesKeys.PROFILE_PICTURE] as! String: ""
            
        }
        
        return(isSuccess,userDetail)
    }

    func registerUser(_ user:User, successCallback: @escaping (String,Bool, String,Bool,User) -> Void, failureCallback:@escaping (NSError)->Void) -> Void {
        let commonRequestParameters = CommonRequestParameters()
        let paramsDictionary:[String:String] = [WebServicesKeys.USER_ID:"0",
                                                WebServicesKeys.FIRST_NAME:user.firstName!,
                                                WebServicesKeys.LAST_NAME:user.lastName!,
                                                WebServicesKeys.EMAIL_ID:user.email!,
                                                WebServicesKeys.PASSWORD:user.password!,
                                                WebServicesKeys.confirmPassword:user.confirmPassword!,
                                                WebServicesKeys.PHONE_NUMBER:user.contactNumber!,
                                                WebServicesKeys.ADDRESS1:user.address1!,
                                                WebServicesKeys.ADDRESS2:user.address2!,
                                                WebServicesKeys.ZIP_CODE: user.zipCode!,
                                                WebServicesKeys.CITY:user.city!,
                                                WebServicesKeys.STATE: user.state!]
        
        print(paramsDictionary)
        UserRegistrationServiceManager.sharedInstance.sendRequestForUserLogin(paramsDictionary as [String : AnyObject], successCallback: {
            response in
            //            let isVerified = response.object(forKey: WebServicesKeys.IS_VERIFIED) as! Bool
            //            let errorCode = "\(response.object(forKey: WebServicesKeys.ERROR_CODE))"
            //            let (isSuceess, message ,userdetails) = self.parseUser(response)
            //            successCallback(errorCode, isSuceess,message,isVerified,userdetails)
            
        },failureCallback: {
            error in
            failureCallback(error)
        })
    }

}
