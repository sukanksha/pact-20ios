//
//  Utilities.swift
//  PACT
//
//  Created by Sukanksha on 26/05/17.
//  Copyright © 2017 Sukanksha. All rights reserved.
//

import Foundation
import Alamofire

class Utilities {
    
    //Check for network connectivity
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
